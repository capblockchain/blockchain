package main

import (
	"crypto/sha512"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"strconv"
	"strings"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

// SimpleChaincode example simple Chaincode implementation
type SimpleChaincode struct {
}

// PaymentInstruction represents a payment instruction which refers to its data using a checksum
type PaymentInstruction struct {
	Emitter        string `json:"emitter"`
	Date           string `json:"date"`
	Checksum       string `json:"checksum"`
	HashFunction   string `json:"hash_function"`
	Network        string `json:"network"`
	Handler        string `json:"handler"`
	HandlingStatus int    `json:"handling_status"`
	HandlingDate   string `json:"handling_date"`
}

// ChecksumsHolder represents the structure that holds all the created checksums
type ChecksumsHolder struct {
	Checksums []string `json:"checksums"`
}

// ChecksumsStateKey represents the key used to store checksums inside the state
const ChecksumsStateKey = "checksums"

// AllowedNetworks represents the allowed payment networks
var AllowedNetworks = [2]string{"sepa", "swift"}

// Init resets all the things
func (t *SimpleChaincode) Init(stub shim.ChaincodeStubInterface, function string, args []string) ([]byte, error) {
	return nil, nil
}

//==============================================================================================================================
//	 Router Functions
//==============================================================================================================================
//	Invoke - Called on chaincode invoke.
//==============================================================================================================================
// Invoke isur entry point to invoke a chaincode function
func (t *SimpleChaincode) Invoke(stub shim.ChaincodeStubInterface, function string, args []string) ([]byte, error) {
	fmt.Println("invoke is running " + function)

	if function == "create_payment_instruction" {
		return t.createPaymentInstruction(stub, args)
	} else if function == "handle_payment_instruction" {
		return t.handlePaymentInstrucction(stub, args)
	} else {
		return nil, errors.New("Received unknown function invocation: " + function)
	}
}

//=================================================================================================================================
//	Query - Called on chaincode query. Takes a function name passed and calls that function. Passes the
//  		initial arguments passed are passed on to the called function.
//=================================================================================================================================
func (t *SimpleChaincode) Query(stub shim.ChaincodeStubInterface, function string, args []string) ([]byte, error) {
	fmt.Println("query is running " + function)
	if function == "ping" {
		return t.ping(stub)
	} else if function == "hash" {
		if len(args) != 2 {
			fmt.Printf("QUERY: Incorrect number of arguments passed to Hash function")
			return nil, errors.New("QUERY: Incorrect number of arguments passed to hash function")
		}

		return hash(args[0], args[1])
	} else if function == "check_hash" {
		if len(args) != 3 {
			fmt.Printf("QUERY: Incorrect number of arguments passed to check_hash function")
			return nil, errors.New("QUERY: Incorrect number of arguments passed to check_hash function")
		}

		return nil, checkHash(args[0], args[1], args[2])
	} else if function == "get_payment_instruction" {
		if len(args) != 1 {
			fmt.Printf("QUERY: Incorrect number of arguments passed to get_payment_instructions function")
			return nil, errors.New("QUERY: Incorrect number of arguments passed to get_payment_instructions function")
		}

		return getPaymentInstruction(stub, args[0])
	} else if function == "list_payment_instructions" {
		if len(args) == 0 {
			fmt.Printf("QUERY: Incorrect number of arguments passed to list_payment_instructions function")
			return nil, errors.New("QUERY: Incorrect number of arguments passed to list_payment_instructions function")
		}

		emitter := args[0]
		handler := args[1]

		status := -1

		if args[2] != "" {
			parsedStatus, err := strconv.Atoi(args[2])

			if err != nil {
				return nil, err
			}

			status = parsedStatus
		}

		return listPaymentInstructions(stub, emitter, handler, status)
	}

	return nil, errors.New("Received unknown function query " + function)

}

func (t *SimpleChaincode) createPaymentInstruction(stub shim.ChaincodeStubInterface, args []string) ([]byte, error) {
	if len(args) != 5 {
		return nil, errors.New("need 5 args (date, checksum, hashFunction, network and handler)")
	}

	date := args[0] // TODO: convert date from string to time
	checksum := args[1]
	hashFunction := args[2]
	network := args[3]
	handler := args[4]

	if date == "" {
		return nil, errors.New("invalid date")
	}

	if checksum == "" {
		return nil, errors.New("invalid checksum")
	}

	if hashFunction == "" {
		return nil, errors.New("invalid hash function")
	}

	if network == "" {
		return nil, errors.New("invalid network")
	}

	if handler == "" {
		return nil, errors.New("invalid handler")
	}

	emitter, err := getAttribute(stub, "username")

	if err != nil {
		fmt.Println(err.Error())
		return nil, errors.New(err.Error())
	}

	if emitter == handler {
		return nil, errors.New("the emitter of a payment instruction cannot handle it")
	}

	fmt.Println("payment instruction emitter: " + emitter)

	err = checkAllowedHandler(stub, handler)

	if err != nil {
		fmt.Println(err.Error())
		return nil, errors.New(err.Error())
	}

	err = checkPaymentNetwork(stub, network)
	if err != nil {
		fmt.Println(err.Error())
		return nil, errors.New(err.Error())
	}

	err = checkCheckSum(stub, checksum)
	if err != nil {
		fmt.Println(err.Error())
		return nil, errors.New(err.Error())
	}

	paymentInstruction := buildPaymentInstruction(emitter, date, checksum, hashFunction, network, handler)

	err = putPaymentInstruction(stub, paymentInstruction, true)
	if err != nil {
		fmt.Println(err.Error())
		return nil, errors.New(err.Error())
	}

	fmt.Println("payment instruction created successfully")

	return nil, nil
}

func buildPaymentInstruction(emitter string, date string, checksum string, hashFunction string, network string, handler string) PaymentInstruction {
	paymentInstruction := PaymentInstruction{}

	paymentInstruction.Emitter = emitter
	paymentInstruction.Date = date
	paymentInstruction.Checksum = checksum
	paymentInstruction.HashFunction = hashFunction
	paymentInstruction.Network = network
	paymentInstruction.Handler = handler

	return paymentInstruction
}

func (t *SimpleChaincode) handlePaymentInstrucction(stub shim.ChaincodeStubInterface, args []string) ([]byte, error) {
	var paymentInstruction PaymentInstruction

	if len(args) != 3 {
		return nil, errors.New("need 3 args (checksum, handling status, and handling date)")
	}

	handler, err := getAttribute(stub, "username")

	checksum := args[0]
	handlingStatus := args[1]
	handlingDate := args[2]

	if checksum == "" {
		return nil, errors.New("invalid checksum")
	}

	if handlingStatus == "" {
		return nil, errors.New("invalid handling status")
	}

	if handlingDate == "" {
		return nil, errors.New("invalid handling date")
	}

	paymentInstructionBytes, err := getPaymentInstruction(stub, checksum)

	err = json.Unmarshal([]byte(paymentInstructionBytes), &paymentInstruction)

	if err != nil {
		fmt.Println(err.Error())
		return nil, errors.New(err.Error())
	}

	if paymentInstruction.Handler != handler {
		msg := "this user cannot handle this payment instruction"
		fmt.Println(msg)
		return nil, errors.New(msg)
	}

	if paymentInstruction.HandlingStatus != 0 {
		msg := "this payment instruction already has a handling status"
		fmt.Println(msg)
		return nil, errors.New(msg)
	}

	handlingStatusInteger, err := strconv.Atoi(handlingStatus)

	if err != nil {
		fmt.Println(err.Error())
		return nil, errors.New(err.Error())
	}

	err = checkAllowedStatus(stub, handlingStatusInteger)

	if err != nil {
		fmt.Println(err.Error())
		return nil, errors.New(err.Error())
	}

	paymentInstruction.HandlingStatus = handlingStatusInteger
	paymentInstruction.HandlingDate = handlingDate

	if err != nil {
		fmt.Println(err.Error())
		return nil, errors.New(err.Error())
	}

	err = putPaymentInstruction(stub, paymentInstruction, false)

	if err != nil {
		fmt.Println(err.Error())
		return nil, errors.New(err.Error())
	}

	fmt.Println("payment instruction handled successfully")

	return nil, nil
}

func putPaymentInstruction(stub shim.ChaincodeStubInterface, paymentInstruction PaymentInstruction, isNew bool) error {
	paymentInstructionBytes, _ := json.Marshal(paymentInstruction)
	err := stub.PutState(paymentInstruction.Checksum, paymentInstructionBytes)

	if err != nil {
		return err
	}

	fmt.Println("payment instruction saved in state successfully")

	if !isNew {
		return nil
	}

	checksumsHolderBytes, err := stub.GetState(ChecksumsStateKey)

	if err != nil {
		return err
	}

	var checksumsHolder ChecksumsHolder
	err = json.Unmarshal(checksumsHolderBytes, &checksumsHolder)

	fmt.Println("payment instructions holder length before update: " + string(len(checksumsHolder.Checksums)))

	checksumsHolder.Checksums = append(checksumsHolder.Checksums, paymentInstruction.Checksum)

	fmt.Println("payment instructions holder length after update: " + string(len(checksumsHolder.Checksums)))

	checksumsHolderBytes, err = json.Marshal(checksumsHolder)

	if err != nil {
		return err
	}

	err = stub.PutState(ChecksumsStateKey, checksumsHolderBytes)

	fmt.Println("payment instructions holder state updated successfully")

	if err != nil {
		return err
	}

	return nil
}

func getPaymentInstruction(stub shim.ChaincodeStubInterface, checksum string) ([]byte, error) {
	paymentInstructionBytes, err := stub.GetState(checksum)

	if err != nil {
		return nil, errors.New(err.Error())
	} else if paymentInstructionBytes == nil {
		msg := "payment instruction does not exist"
		fmt.Println(msg)
		return nil, errors.New(msg)
	}

	return paymentInstructionBytes, err
}

func listPaymentInstructions(stub shim.ChaincodeStubInterface, emitter string, handler string, status int) ([]byte, error) {
	checksumsHolderBytes, err := stub.GetState(ChecksumsStateKey)

	if err != nil {
		return nil, err
	}

	var checksumsHolder ChecksumsHolder
	err = json.Unmarshal(checksumsHolderBytes, &checksumsHolder)

	if err != nil {
		return nil, err
	}

	fmt.Println("payment instructions holder length: " + string(len(checksumsHolder.Checksums)))

	var paymentInstructionsList []PaymentInstruction

	for _, checksum := range checksumsHolder.Checksums {
		paymentInstructionBytes, err := getPaymentInstruction(stub, checksum)

		if err != nil {
			return nil, err
		}

		var paymentInstruction PaymentInstruction
		err = json.Unmarshal(paymentInstructionBytes, &paymentInstruction)

		if err != nil {
			return nil, err
		}

		isPaymentInstructionOk := true

		if emitter != "" && handler != "" {
			isPaymentInstructionOk = (emitter == paymentInstruction.Emitter && handler == paymentInstruction.Handler)
		} else if emitter != "" {
			isPaymentInstructionOk = (emitter == paymentInstruction.Emitter)
		} else if handler != "" {
			isPaymentInstructionOk = (handler == paymentInstruction.Handler)
		}

		if status != -1 && status != paymentInstruction.HandlingStatus {
			isPaymentInstructionOk = false
		}

		if isPaymentInstructionOk {
			paymentInstructionsList = append(paymentInstructionsList, paymentInstruction)

			fmt.Println("payment instructions list length: " + string(len(paymentInstructionsList)))
		}
	}

	paymentInstructionsListBytes, err := json.Marshal(paymentInstructionsList)

	if err != nil {
		return nil, err
	}

	return paymentInstructionsListBytes, nil
}

// You can define any workflow based on statuses here.
// We currently simply verify that the status is within a specific range.
func checkAllowedStatus(stub shim.ChaincodeStubInterface, status int) error {
	if status != 0 && status != 1 && status != 2 && status != 3 {
		return errors.New("the handling status must be either 0, 1, 2 or 3")
	}

	return nil
}

// You can define allowed handlers for an emitter in the certificate attributes (current implementation).
// However, this is currently disabled to make the process easier.
func checkAllowedHandler(stub shim.ChaincodeStubInterface, handler string) error {
	return nil // remove this line to turn on handlers check

	allowedHandlers, err := getAttribute(stub, "allowed_handlers")

	if err != nil {
		fmt.Println(err.Error())
		return errors.New(err.Error())
	}

	fmt.Println("allowed handlers: " + allowedHandlers)

	allowedHandlersArray := strings.Split(allowedHandlers, ";")
	isHandlerAllowed := false

	for i := 0; i < len(allowedHandlersArray); i++ {
		if allowedHandlersArray[i] == handler {
			isHandlerAllowed = true
		}
	}

	if !isHandlerAllowed {
		msg := handler + " is not an allowed handler (allowed: " + allowedHandlers + ")"
		fmt.Println(msg)
		return errors.New(msg)
	}

	return nil
}

func checkPaymentNetwork(stub shim.ChaincodeStubInterface, network string) error {
	for i := 0; i < len(AllowedNetworks); i++ {
		if AllowedNetworks[i] == network {
			return nil
		}
	}

	return errors.New(network + " is not an allowed payment network")
}

func checkCheckSum(stub shim.ChaincodeStubInterface, checksum string) error {
	paymentInstructionBytes, err := stub.GetState(checksum)
	if err != nil {
		return nil
	} else if paymentInstructionBytes == nil {
		msg := "checkCheckSum : OK - Payment instruction does not exist"
		fmt.Println(msg)
		return nil
	}
	msg := checksum + "payment instruction already exist [CAN'T SAVE]"
	return errors.New(msg)
}

//=================================================================================================================================
//	 Ping Function
//=================================================================================================================================
//	 Pings the peer to keep the connection alive
//=================================================================================================================================
func (t *SimpleChaincode) ping(stub shim.ChaincodeStubInterface) ([]byte, error) {
	return []byte("Hello, from Blockbuster team!"), nil
}

//=================================================================================================================================
//	 Hash Functions
//=================================================================================================================================
//
//=================================================================================================================================
func hash(data string, hashFunction string) ([]byte, error) {
	if hashFunction == "sha512" {
		return hashWithsha512(data), nil
	}

	return nil, errors.New(hashFunction + " is not a valid hash function")
}

func checkHash(data string, hash string, hashFunction string) error {
	if hashFunction == "sha512" {
		encoded := hashWithsha512(data)
		if hash == string(encoded) {
			return nil
		}
	}
	return errors.New(hashFunction + " is not a valid hash function")
}

func hashWithsha512(data string) []byte {
	h := sha512.New()
	io.WriteString(h, data)
	return h.Sum(nil)
}

//=================================================================================================================================
//	 Main - main - Starts up the chaincode
//=================================================================================================================================
func main() {

	err := shim.Start(new(SimpleChaincode))
	if err != nil {
		fmt.Printf("Error starting Simple chaincode: %s", err)
	}
}

//=================================================================================================================================
//	 Utils - utilitary functions
//=================================================================================================================================
func getAttribute(stub shim.ChaincodeStubInterface, attributeName string) (string, error) {
	bytes, err := stub.ReadCertAttribute(attributeName)
	return string(bytes[:]), err
}
